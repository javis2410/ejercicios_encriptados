﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace root13
{
    public partial class Form1 : Form
    {
        char[] minusculas = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        public Form1()
        {
            InitializeComponent();
        }
        public void encriptar(string cadena)
        {
            label1.Text = "";
            char[] letras = cadena.ToArray();
            for (int i = 0; i < letras.Length; i++)
            {
                for (int j = 0; j < minusculas.Length; j++)
                {
                    if (letras[i] == minusculas[j])
                    {
                        if (j + 13 <= 26)
                            j = j + 13;
                        else
                        {
                            j = j + 13;
                            j = j - 26;
                        }
                        letras[i] = minusculas[j];
                        label1.Text += letras[i];
                        break;
                    }
                }
            }
        }
        public void desencriptar(string cadena)
        {
            label2.Text = "";
            char[] letras = cadena.ToArray();
            for (int i = 0; i < letras.Length; i++)
            {
                for (int j = 0; j < minusculas.Length; j++)
                {
                    if (letras[i] == minusculas[j])
                    {
                        if (j - 13>=0)
                            j = j - 13;
                        else
                        {
                            j = 13 - j;
                            j = 26 - j;
                        }
                        letras[i] = minusculas[j];
                        label2.Text += letras[i];
                        break;
                    }
                }
            }
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            string palabra = textBox1.Text;
            encriptar(palabra);
        }

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            string palabra = textBox2.Text;
            desencriptar(palabra);
        }
    }
}
