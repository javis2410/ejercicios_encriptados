﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Transposicion
{
    public partial class Form1 : Form
    {
        char[] letras;
        char[] movimientos;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            string cadena = textBox1.Text;
            Encriptar(cadena);
        }
        public void Encriptar(string cadena)
        {
            label1.Text = "";
            letras = cadena.ToArray();
            movimientos = cadena.ToArray();
            if (letras.Length > 1)
            {
                for (int i = 0; i < letras.Length; i++)
                {
                    if (i < letras.Length - 1)
                        movimientos[i] = letras[i + 1];
                    else
                        movimientos[i] = letras[0];
                    label1.Text += movimientos[i];
                }
            }
        }
        public void Desencriptar(string cadena)
        {
            int contador = 0;
            label2.Text = "";
            letras = cadena.ToArray();
            movimientos = cadena.ToArray();
            if (letras.Length > 1)
            {
                for (int i = 0; i < letras.Length; i++)
                {
                    if (i == 0)
                        letras[i] = movimientos[movimientos.Length - 1];
                    else
                    {
                        letras[i] = movimientos[contador];
                        contador++;
                    }
                
                    label2.Text += letras[i];

                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string cadena = textBox2.Text;
            Desencriptar(cadena);
        }
    }
}
