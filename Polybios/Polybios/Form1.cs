﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Polybios
{
    public partial class Form1 : Form
    {
        char[] letras;
        string letra;
        char[,] encriptado = new char[5, 5] {
            { 'a','b','c','d','e'},
            { 'f', 'g', 'h', 'i', 'j' },
            { 'k', 'l', 'm', 'n', 'o' },
            { 'p', 'q', 'r', 's', 't' },
            { 'u', 'v', 'w', 'x', 'y' } };
        string[] des;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            string cadena = textBox1.Text;
            encriptar(cadena);
        }
        public void encriptar(string cadena)
        {
            label1.Text = "";
            letras = cadena.ToArray();
            for (int i = 0; i < letras.Length; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    for (int k = 0; k < 5; k++)
                    {
                        if (letras[i] == encriptado[j, k])
                        {
                            if (j == 0 && k == 0)
                                label1.Text += "aa";
                            if (j == 0 && k == 1)
                                label1.Text += "ab";
                            if (j == 0 && k == 2)
                                label1.Text += "ac";
                            if (j == 0 && k == 3)
                                label1.Text += "ad";
                            if (j == 0 && k == 4)
                                label1.Text += "ae";

                            if (j == 1 && k == 0)
                                label1.Text += "ba";
                            if (j == 1 && k == 1)
                                label1.Text += "bb";
                            if (j == 1 && k == 2)
                                label1.Text += "bc";
                            if (j == 1 && k == 3)
                                label1.Text += "bd";
                            if (j == 1 && k == 4)
                                label1.Text += "be";

                            if (j == 2 && k == 0)
                                label1.Text += "ca";
                            if (j == 2 && k == 1)
                                label1.Text += "cb";
                            if (j == 2 && k == 2)
                                label1.Text += "cc";
                            if (j == 2 && k == 3)
                                label1.Text += "cd";
                            if (j == 2 && k == 4)
                                label1.Text += "ce";

                            if (j == 3 && k == 0)
                                label1.Text += "da";
                            if (j == 3 && k == 1)
                                label1.Text += "db";
                            if (j == 3 && k == 2)
                                label1.Text += "dc";
                            if (j == 3 && k == 3)
                                label1.Text += "dd";
                            if (j == 3 && k == 4)
                                label1.Text += "de";

                            if (j == 4 && k == 0)
                                label1.Text += "ea";
                            if (j == 4 && k == 1)
                                label1.Text += "eb";
                            if (j == 4 && k == 2)
                                label1.Text += "ec";
                            if (j == 4 && k == 3)
                                label1.Text += "ed";
                            if (j == 4 && k == 4)
                                label1.Text += "ee";
                        }
                    }
                }
            }
            label1.Text += letra;
        }

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            string cadena = textBox2.Text;
            desencriptar(cadena);
        }
        void desencriptar(string cadena)
        {
            label2.Text = "";
            int c = 0;
            letras = cadena.ToArray();
            int n = letras.Length / 2;
            des = new string[n];
            for (int i = 0; i < letras.Length - 1; i++)
            {
                if ((i % 2) == 0)
                {
                    des[c] = Convert.ToString(letras[i]) + Convert.ToString(letras[i + 1]);
                    if (des[c] == "aa")
                        des[c] = "a";
                    if (des[c] == "ab")
                        des[c] = "b";
                    if (des[c] == "ac")
                        des[c] = "c";
                    if (des[c] == "ad")
                        des[c] = "d";
                    if (des[c] == "ae")
                        des[c] = "e";

                    if (des[c] == "ba")
                        des[c] = "f";
                    if (des[c] == "bb")
                        des[c] = "g";
                    if (des[c] == "bc")
                        des[c] = "h";
                    if (des[c] == "bd")
                        des[c] = "i";
                    if (des[c] == "be")
                        des[c] = "j";

                    if (des[c] == "ca")
                        des[c] = "k";
                    if (des[c] == "cb")
                        des[c] = "l";
                    if (des[c] == "cc")
                        des[c] = "m";
                    if (des[c] == "cd")
                        des[c] = "n";
                    if (des[c] == "ce")
                        des[c] = "o";

                    if (des[c] == "da")
                        des[c] = "p";
                    if (des[c] == "db")
                        des[c] = "q";
                    if (des[c] == "dc")
                        des[c] = "r";
                    if (des[c] == "dd")
                        des[c] = "s";
                    if (des[c] == "de")
                        des[c] = "t";

                    if (des[c] == "ea")
                        des[c] = "u";
                    if (des[c] == "eb")
                        des[c] = "v";
                    if (des[c] == "ec")
                        des[c] = "w";
                    if (des[c] == "ed")
                        des[c] = "x";
                    if (des[c] == "ee")
                        des[c] = "y";
                    label2.Text += des[c];
                    c++;
                }
            }
        }
    }
}

